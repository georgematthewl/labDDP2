package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.*;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.io.*;
import java.util.Scanner;

import static xoxo.key.HugKey.DEFAULT_SEED;


/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author George Matthew Limongan
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(e -> encryptMessage());
        gui.setDecryptFunction(e -> decryptMessage());
        gui.setBrowseFunction(e -> browseFile());
    }

    /**
     * Method to encrypt the message.
     */

    public void encryptMessage() {
        String messageText = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption encryptMsg = null;
        try {
            encryptMsg = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
        }
        XoxoMessage message = null;
        try {
            if (seed.equals("") || Integer.parseInt(seed) == 18) {
                message = encryptMsg.encrypt(messageText);
                seed = "18";
            } else {
                message = encryptMsg.encrypt(messageText, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must between 0 and 36 (inclusive)");
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be a number!");
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size too big (more than 10Kbit)!");
        } catch (NullPointerException e) {
            gui.setWarning("Null pointer! Please check your program again.");
        }
        encryptedFileWrite(message, seed);
    }

    /**
     * Method to decrypt the message.
     */

    public void decryptMessage() {
        String hugKey = gui.getKeyText();
        String encryptedMessage = gui.getMessageText();
        XoxoDecryption decryptMsg = new XoxoDecryption(hugKey);
        int seed;
        if (gui.getSeedText().equals("") || Integer.parseInt(gui.getSeedText()) == 18) {
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        String decryptedMessage = decryptMsg.decrypt(encryptedMessage, seed);
        decryptedFileWrite(decryptedMessage);
    }

    /**
     * Method to write message that has been decrypted to some folder.
     * @param decryptMsg message that already decrypted
     */

    public void decryptedFileWrite(String decryptMsg) {
        File file = new File("lab_10/src/main/files/decrypted/fileDecrypted.txt");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file, true);
            writer.write(decryptMsg + System.lineSeparator());
            writer.flush();
            writer.close();
            gui.appendLog("Decrypted text: " + decryptMsg);
        } catch (IOException e) {
            gui.appendLog("Decrypt failed! Cannot write file.");
        }
    }

    /**
     * Method to write message that has been encrypted to some folder.
     * @param encryptMsg message that already encrypted
     * @param seed seed of the encrypted message.
     */

    public void encryptedFileWrite(XoxoMessage encryptMsg, String seed) {
        File file = new File("lab_10/src/main/files/encrypted/fileEncrypted.enc");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file, true);
            writer.write(encryptMsg.getEncryptedMessage() + " " + encryptMsg.getHugKey().getKeyString() + " " + seed + System.lineSeparator());
            writer.flush();
            writer.close();
            gui.appendLog("Encrypted text: " + encryptMsg.getEncryptedMessage());
            gui.appendLog("Hug key: " + encryptMsg.getHugKey().getKeyString());
            gui.appendLog("Seed: " + seed + System.lineSeparator());
        } catch (IOException e) {
            gui.appendLog("Encrypt failed! Cannot create file.");
        }

    }

    /**
     * Method to browse the file to encrypt/decrypt.
     */

    public void browseFile() {
        File file = new File("lab_10/src/main/files/");
        JFileChooser jfc = new JFileChooser(file);
        jfc.setDialogTitle("File Selector");
        jfc.setMultiSelectionEnabled(true);
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File[] files = jfc.getSelectedFiles();
            processFile(files);
        }
    }

    /**
     * Process the file that chosen then decrypt the messages.
     * @param files file that chose
     */

    public void processFile(File[] files) {
        for (File file : files) {
            try {
                Scanner input = new Scanner(file);
                while (input.hasNextLine()) {
                    String line = input.nextLine();
                    String[] splitInput = line.split(" ");
                    String encryptedMessage = splitInput[0];
                    String hugKey = splitInput[1];
                    String seedStr = splitInput[2];
                    XoxoDecryption decryptMsg = new XoxoDecryption(hugKey);
                    int seed;
                    if (seedStr.equals("") || Integer.parseInt(seedStr) == 18 || seedStr.equals("18")) {
                        seed = DEFAULT_SEED;
                    } else {
                        seed = Integer.parseInt(seedStr);
                    }
                    String decryptedMessage = decryptMsg.decrypt(encryptedMessage, seed);
                    decryptedFileWrite(decryptedMessage);
                }
            } catch (FileNotFoundException e) {
                gui.setWarning("File not found! Please check again!");
            }
        }
    }

}
package xoxo.exceptions;

/**
 * An exception that is thrown if character that is inputed
 * contain other than A-Z, a-z, and @ character.
 */
public class InvalidCharacterException extends RuntimeException {

    public InvalidCharacterException(String message) {
        super(message);
    }

}
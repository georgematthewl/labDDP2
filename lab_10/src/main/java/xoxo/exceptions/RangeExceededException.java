package xoxo.exceptions;

/**
 * An exception that is thrown if seed value is not between 0 and 36.
 */
public class RangeExceededException extends RuntimeException {

    /**
     * Class constructor.
     *
     * @param message text that will be shown if this exception is thrown.
     */

    public RangeExceededException(String message) {
        super(message);
    }
}
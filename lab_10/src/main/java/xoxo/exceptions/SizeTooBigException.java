package xoxo.exceptions;

/**
 * An exception that is thrown if the size of the messages are over 10Kbit.
 */
public class SizeTooBigException extends RuntimeException {

    /**
     * Class constructor.
     *
     * @param message text that will be shown if this exception is thrown.
     */

    public SizeTooBigException(String message) {
        super(message);
    }
}
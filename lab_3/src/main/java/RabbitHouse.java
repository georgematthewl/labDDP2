import java.util.Scanner;

public class RabbitHouse {
	public static int hitungPalindrom(String str) {
		if (isPalindrom(str)) {
			return 0;
		}
		int count = 1;
		for (int i = 0; i < str.length(); i++) {
			String front = str.substring(0, i);
			String back = str.substring(i + 1);
			if (!isPalindrom(front + back)) {
				count += hitungPalindrom(front + back);
			}
		}
		return count;
	}

	public static int hitungRabbit(int panjang, int jumlah) {
		if (panjang == 1) {
			return 1;
		} else {
			return (panjang * jumlah + hitungRabbit(panjang - 1, panjang * jumlah));
		}
	}

	public static boolean isPalindrom(String str) {
		for (int i = 0; i < (str.length() / 2); i++) {
			if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String masukan = input.next();
		String nama = input.next();
		int panjang = nama.length();

		if (masukan.equals("palindrom")) {
			System.out.println(hitungPalindrom(nama));
		} else if (masukan.equals("normal")) {
			System.out.println(hitungRabbit(panjang, 1));
		} else {
			System.out.println("FORMAT YANG DIMASUKAN SALAH!");
			System.exit(0);
		}

		input.close();
	}

}

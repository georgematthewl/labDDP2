package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Class representing event managing system
 */

public class EventSystem {

    /**
     * List of events
     */

    private ArrayList<Event> events;

    /**
     * List of users
     */

    private ArrayList<User> users;

    /**
     * Constructor. 
     * Initializes events and users with empty lists.
     */

    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method to find a user.
     * @param name      name of user that is going to be searched.
     * @return user
     */

    public User findUser(String name) {
        for (User user : users) {
            if (user.getName().equalsIgnoreCase(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Method to find an event.
     * 
     * @param eventName   name of event that is going to be searched.
     * @return event name
     */
    
    public Event findEvent(String eventName) {
        for (Event event : events) {
            if (event.getName().equalsIgnoreCase(eventName)) {
                return event;
            }
        }
        return null;
    }

    /**
     * Method to add an event if the condition fulfilled.
     * @param name              name of event
     * @param startTimeStr      time the event begin
     * @param endTimeStr        time the event ended
     * @param costPerHourStr    the cost of an event per hour
     * @return String depends on the condition fulfilled.
     */

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event eventName = findEvent(name);
        if (eventName == null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
            LocalDateTime beginTime = LocalDateTime.parse(startTimeStr, formatter);
            LocalDateTime endTime = LocalDateTime.parse(endTimeStr, formatter);
            if (beginTime.isAfter(endTime)) {
                return "Waktu yang diinputkan tidak valid!";
            }
            events.add(new Event(name, beginTime, endTime, new BigInteger(costPerHourStr)));
            return "Event " + name + " berhasil ditambahkan!";
        }
        return "Event " + name + " sudah ada!";
    }

    /**
     * Method to add user.
     * @param name      name of user that is going to be added
     * @return String that depends on the condition fulfilled.
     */

    public String addUser(String name) {
        User userName = findUser(name);
        if (userName == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }
    
    /**
     * Method to register user to an event.
     * @param userName  the name of user
     * @param eventName the name of event
     * @return String that depends on the condition fulfilled
     */

    public String registerToEvent(String userNameStr, String eventNameStr) {
        User userName = findUser(userNameStr);
        Event eventName = findEvent(eventNameStr);
        if (userName == null && eventName == null) {
            return "Tidak ada pengguna dengan nama " + userNameStr + " dan acara dengan nama " + eventNameStr + "!";
        } else if (userName == null) {
            return "Tidak ada pengguna dengan nama " + userNameStr + "!";
        } else if (eventName == null) {
            return "Tidak ada acara dengan nama " + eventNameStr + "!";
        } else if (userName.addEvent(eventName)) {
            return userNameStr + " berencana menghadiri " + eventNameStr + "!";
        } else {
            return userNameStr + " sibuk sehingga tidak dapat menghadiri " + eventNameStr + "!";
        }
    }
    
    /**
     * Method to get an event and print toString.
     * @param   eventName name of an event
     * @return String depends on the condition fulfilled
     */

    public String getEvent(String eventNameStr) {
        Event eventName = findEvent(eventNameStr);
        if (eventName != null) {
            return eventName.toString();
        } else {
            return "Tidak ada event " + eventNameStr;
        }
    }
    
    /**
     * Method to get an object of user.
     * @param name      name of user
     * @return  object of user searched
     */

    public User getUser(String name) {
        return findUser(name);
    }

}
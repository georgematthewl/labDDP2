package lab9.event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


/**
 * A class representing an event and its properties
 */

public class Event implements Comparable<Event> {
    /** Name of event */
    private String name;

    /**
     * Instance variables for representing beginning and end time of event.
     */
    private LocalDateTime beginTime, endTime;

    /**
     * Instance variable for cost per hour.
     */
    
    private BigInteger cost;

    /**
     * Constructor for Event.
     * @param name              name of the event
     * @param beginTime         time of the event start
     * @param endTime           time of the event finish
     * @param costPerHour       cost of an event per hour
     */

    public Event(String name, LocalDateTime beginTime, LocalDateTime endTime, BigInteger cost) {
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.cost = cost;
    }

    /**
     * Accessor for name field.
     * 
     * @return name of this event instance
     */

    public String getName() {
        return this.name;
    }
    
    /**
     * Accessor for cost.
     * @return cost of the event in BigInteger
     */
    
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Override method toString().
     * return event information
     */

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return name + "\n" + "Waktu mulai: " + beginTime.format(formatter) + "\n" + "Waktu selesai: "
                + endTime.format(formatter) + "\n" + "Biaya kehadiran: " + cost;
    }

    /**
     * Implement method compareTo from Comparable interface.
     */

    @Override
    public int compareTo(Event otherEvent) {
        return (int) otherEvent.beginTime.until(beginTime, ChronoUnit.SECONDS);
    }

    /**
     * Method to check an event overlaps with other event or not.
     * @param   other event to be checked
     * @return  false if overlapped, true otherwise
     */

    public boolean notOverlapsWith(Event otherEvent) {
        return (endTime.isBefore(otherEvent.beginTime) || otherEvent.endTime.isBefore(beginTime));
    }
}

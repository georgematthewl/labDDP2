import java.util.Scanner;

public class BingoGame {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int banyakPemain = input.nextInt();

        /* Membuat list yang berisi nama pemain */
        String[] listNama = new String[banyakPemain];
        for (int i = 0; i < banyakPemain; i++) {
            String nama = input.next();
            listNama[i] = nama;

        }
        input.nextLine();
        Player[] listPlayer = new Player[banyakPemain]; // Membuat array yang panjangnya sebanyak jumlah pemain
        for (int player = 0; player < banyakPemain; player++) {
            Number[][] listAngka = new Number[5][5]; // Membuat multidimensional array yang baru setiap loop pemain
            for (int i = 0; i < 5; i++) {
                String[] inputSplit = input.nextLine().split(" ");
                for (int j = 0; j < 5; j++) {
                    int num = Integer.parseInt(inputSplit[j]);
                    Number number = new Number(num, i, j);
                    listAngka[i][j] = number;
                }
            }
            BingoCard bingoCard = new BingoCard(listAngka); // Membuat kartu dan memasukan list angka
            Player pemain = new Player(listNama[player], bingoCard, false, false); // Membuat objek pemain
            listPlayer[player] = pemain; // Memasukan objek pemain ke dalam listPlayer
        }

        String method;
        int hitungMenang = 0;
        while ((method = input.nextLine()) != null) {
            String[] splitInput = method.split(" ");
            if (splitInput[0].equalsIgnoreCase("MARK")) {
                for (Player nama : listPlayer) {
                    if (nama.isWin()) {
                        System.out.println(nama.getName() + " sudah menang dan tidak dapat bermain lagi");
                    } else {
                        System.out.print("Player " + nama.getName() + ": ");
                        int inputAngka = input.nextInt();
                        System.out.println(nama.getCard().markNum(inputAngka, nama.getName()));
                        input.nextLine();
                        if (nama.getCard().isBingo()) {
                            System.out.println(nama.getName() + " mendapatkan BINGO!");
                            System.out.println(nama.getCard().info());
                            nama.setWin(true);
                            hitungMenang += 1;
                        }
                    }

                }
                if (hitungMenang == banyakPemain) {
                    System.out.println("Semua pemain sudah menang! Permainan selesai");
                    System.exit(0);
                }

            } else if (splitInput[0].equalsIgnoreCase("INFO")) {
                for (int i = 0; i < banyakPemain; i++) {
                    if (splitInput[1].equalsIgnoreCase(listPlayer[i].getName())) {
                        System.out.println(listPlayer[i].getName());
                        System.out.println(listPlayer[i].getCard().info());
                    }
                }
            } else if (splitInput[0].equalsIgnoreCase("RESTART")) {
                for (int i = 0; i < banyakPemain; i++) {
                    if (splitInput[1].equalsIgnoreCase(listPlayer[i].getName())) {
                        if (listPlayer[i].isWin()) {
                            System.out.println(listPlayer[i].getName() + " sudah menang dan tidak dapat bermain lagi");
                        } else {
                            if (!listPlayer[i].isRestarted()) {
                                listPlayer[i].getCard().restart();
                                listPlayer[i].setRestarted(true);

                            } else {
                                System.out.println(listPlayer[i].getName() + " sudah pernah mengajukan RESTART");
                            }
                        }
                    }
                }
            } else {
                System.out.println("Incorrect command");
            }

        }
        input.close();
    }

}

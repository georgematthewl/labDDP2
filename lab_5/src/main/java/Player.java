
public class Player {
    private String name;
    private BingoCard card;
    private boolean isRestarted;
    private boolean isWin;

    public Player(String name, BingoCard card, boolean isRestarted, boolean isWin) {
        this.name = name;
        this.card = card;
        this.isRestarted = false;
        this.isWin = false;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean isWin) {
        this.isWin = isWin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BingoCard getCard() {
        return card;
    }

    public void setCard(BingoCard card) {
        this.card = card;
    }

    public boolean isRestarted() {
        return isRestarted;
    }

    public void setRestarted(boolean isRestarted) {
        this.isRestarted = isRestarted;
    }

}

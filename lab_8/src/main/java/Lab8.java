import java.util.Scanner;
import Corporate.Corporate;

public class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int max_salary = Integer.parseInt(input.nextLine());
            if (max_salary > 0) {
                Corporate.setMax_salary(max_salary);
                System.out.println("Batas gaji maksimal STAFF: " + max_salary);
            }
            while (input.hasNextLine()) {
                String[] input_split = input.nextLine().split(" ");
                if (input_split[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                    Corporate.add(input_split[2], input_split[1].toUpperCase(), Integer.parseInt(input_split[3]));
                } else if (input_split[0].toUpperCase().equals("STATUS")) {
                    Corporate.status(input_split[1]);
                } else if (input_split[0].toUpperCase().equals("GAJIAN")) {
                    Corporate.payDay();
                } else if (input_split[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                    Corporate.addJunior(input_split[1], input_split[2]);
                } else {
                    System.out.println("FORMAT MASUKAN SALAH!");
                }
            }
        } catch (Exception e) {
            System.out.println("FORMAT MASUKAN SALAH!");
        }
    }
}

package Corporate.Employee;

import java.util.ArrayList;

import Corporate.Employee.Manager.Manager;
import Corporate.Employee.Staff.Staff;
import Corporate.Employee.Intern.Intern;

public class Employee {
    private String name;
    private int salary;
    private int salaryCounter = 0;
    private int maxJunior;
    private ArrayList<Employee> junior = new ArrayList<Employee>();
    private static final int MIN_SALARY_RECEIVED_BEFORE_RAISE = 6;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
        if (this instanceof Staff || this instanceof Manager) {
            this.maxJunior = 10;
        } else if (this instanceof Intern) {
            this.maxJunior = 0;
        }
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public int getSalaryCounter() {
        return salaryCounter;
    }

    public void setSalaryCounter(int salaryCounter) {
        this.salaryCounter = salaryCounter;
    }

    public ArrayList<Employee> getJunior() {
        return junior;
    }

    public void setJunior(ArrayList<Employee> junior) {
        this.junior = junior;
    }

    public int getMaxJunior() {
        return maxJunior;
    }

    public void addJunior(Employee employee) {
        junior.add(employee);
    }

    public boolean canRecruit(Employee employee) {
        if (this instanceof Manager) {
            if (employee instanceof Staff || employee instanceof Intern) {
                return true;
            }
        } else if (this instanceof Staff) {
            if (employee instanceof Intern) {
                return true;
            }
        }
        return false;
    }

    public void payDay() {
        salaryCounter++;
        if (salaryCounter == MIN_SALARY_RECEIVED_BEFORE_RAISE) {
            this.setSalaryCounter(0);
            raiseSalary();
        }
    }

    public void raiseSalary() {
        int newSalary = (salary / 10) + salary;
        System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari " + salary + " menjadi " + newSalary);
        salary = newSalary;
    }

}

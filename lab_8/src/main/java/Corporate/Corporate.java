package Corporate;

import java.util.ArrayList;
import Corporate.Employee.*;
import Corporate.Employee.Manager.Manager;
import Corporate.Employee.Staff.Staff;
import Corporate.Employee.Intern.Intern;

public class Corporate {
    private static final int MAX_EMPLOYEE = 10000;
    private static ArrayList<Employee> employer = new ArrayList<Employee>();
    private static int max_salary;

    public static Employee find(String name) {
        for (Employee emp : employer) {
            if (name.equalsIgnoreCase(emp.getName())) {
                return emp;
            }
        }
        return null;
    }

    public static void add(String position, String name, int salary) {
        Employee employee = find(name);
        if (employer.size() <= MAX_EMPLOYEE) {
            if (!check(employee)) {
                switch (position) {
                    case "MANAGER":
                        employer.add(new Manager(name, salary));
                        break;
                    case "STAFF":
                        employer.add(new Staff(name, salary));
                        break;
                    case "INTERN":
                        employer.add(new Intern(name, salary));
                        break;
                }
                System.out.println(name + " mulai bekerja sebagai " + position + " di PT TAMPAN");
            } else {
                System.out.println("Karyawan dengan nama " + name + " telah terdaftar");
            }
        } else {
            System.out.println("Jumlah karyawan PT TAMPAN sudah penuh..");
        }
    }

    public static void status(String name) {
        Employee employee = find(name);
        if (check(employee)) {
            System.out.println(employee.getName() + " " + employee.getSalary());
        } else {
            System.out.println("Karyawan tidak terdaftar");
        }
    }

    public static void addJunior(String emp, String juniorMember) {
        Employee me = find(emp);
        Employee junior = find(juniorMember);
        if (check(me) && check(junior)) {
            if ((me.getJunior().size() <= me.getMaxJunior()) && me.canRecruit(junior)) {
                if (me.getJunior().contains(junior)) {
                    System.out.println("Karyawan " + juniorMember + " telah menjadi bawahan " + emp);
                } else {
                    me.addJunior(junior);
                    System.out.println("Karyawan " + juniorMember + " berhasil ditambahkan menjadi bawahan " + emp);
                }
            } else {
                if (me.getJunior().size() > me.getMaxJunior()) {
                    System.out.println("Jumlah bawahan Anda sudah melebihi batas maksimal");
                } else if (!me.canRecruit(junior)) {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            }
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    public static void payDay() {
        if (employer.size() == 0) {
            System.out.println("Belum ada karyawan");
        } else {
            String print = "";
            int count = 0;
            for (Employee emp : employer) {
                emp.payDay();
                if (emp instanceof Staff && emp.getSalary() > max_salary) {
                    Manager newManager = new Manager(emp.getName(), emp.getSalary());
                    newManager.setJunior(emp.getJunior());
                    employer.set(employer.indexOf(emp), newManager);
                    print = "Selamat, " + emp.getName() + " telah dipromosikan menjadi MANAGER";
                    count++;
                }
            }
            System.out.println("Semua karyawan telah diberikan gaji");
            if (count != 0) {
                System.out.println(print);
            }
        }
    }

    public static boolean check(Employee emp) {
        if (emp == null)
            return false;
        else
            return true;
    }

    public static int getMax_salary() {
        return max_salary;
    }

    public static void setMax_salary(int max_salary) {
        Corporate.max_salary = max_salary;
    }

}

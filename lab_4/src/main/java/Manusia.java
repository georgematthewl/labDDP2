import java.util.ArrayList;

public class Manusia {

	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;

	private static ArrayList<Manusia> listNama = new ArrayList<Manusia>();

	// membuat constructor
	public Manusia(String nama, int umur, int uang) {
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
		listNama.add(this);
	}

	public Manusia(String nama, int umur) {
		this(nama, umur, 50000);
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getUmur() {
		return umur;
	}

	public void setUmur(int umur) {
		this.umur = umur;
	}

	public int getUang() {
		return uang;
	}

	public void setUang(int uang) {
		this.uang = uang;
	}

	public float getKebahagiaan() {
		return kebahagiaan;
	}

	public void setKebahagiaan(float kebahagiaan) {
		this.kebahagiaan = kebahagiaan;
	}

	public void beriUang(Manusia penerima) {
		int jumlah = 0;
		char huruf;
		for (int counter = 0; counter < penerima.getNama().length(); counter++) {
			huruf = penerima.getNama().charAt(counter);
			int ascii = (int) huruf;
			jumlah += ascii;
		}
		int jumlahUang = jumlah * 100;
		this.beriUang(penerima, jumlahUang);
	}

	public void beriUang(Manusia penerima, int jumlah) {
		if (!listNama.contains(penerima) && !listNama.contains(this)) {
			System.out.println(nama + " dan " + penerima.getNama() + " telah meninggal :(");
			return;
		}
		if (!(listNama.contains(this))) {
			System.out.println(nama + " telah tiada");
			return;
		}
		if (!(listNama.contains(penerima))) {
			System.out.println(penerima.getNama() + " telah tiada");
			return;
		}
		if (jumlah < uang) {
			uang -= jumlah;
			penerima.setUang(penerima.getUang() + jumlah);
			System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama()
					+ ", mereka berdua senang :D");
			float kebahagiaanBaru = (float) (jumlah / 6000.0);
			kebahagiaan += kebahagiaanBaru;
			if (kebahagiaan > 100) {
				kebahagiaan = 100;
			}
			penerima.setKebahagiaan(penerima.getKebahagiaan() + kebahagiaanBaru);
			if (penerima.getKebahagiaan() > 100) {
				penerima.setKebahagiaan(100);
			}
		} else {
			System.out.println(
					nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
		}
	}

	public void bekerja(int durasi, int bebanKerja) {
		if (!(listNama.contains(this))) {
			System.out.println(nama + " telah tiada");
			return;
		}
		if (umur < 18) {
			System.out.println(nama + " belum boleh bekerja karena masih di bawah umur D:");
		} else {
			int bebanKerjaTotal = durasi * bebanKerja;
			int pendapatan;
			if (bebanKerjaTotal <= kebahagiaan) {
				kebahagiaan -= bebanKerjaTotal;
				pendapatan = bebanKerjaTotal * 10000;
				System.out.println(nama + " bekerja full time, total pendapatan: " + pendapatan);
				uang += pendapatan;
			} else if (bebanKerjaTotal > kebahagiaan) {
				int durasiBaru = (int) (kebahagiaan / bebanKerja);
				bebanKerjaTotal = durasiBaru * bebanKerja;
				pendapatan = bebanKerjaTotal * 10000;
				kebahagiaan -= bebanKerjaTotal;
				if (kebahagiaan < 0) {
					kebahagiaan = 0;
				}
				System.out
						.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: "
								+ pendapatan);
				uang += pendapatan;
			}
		}
	}

	public void rekreasi(String namaTempat) {
		if (!(listNama.contains(this))) {
			System.out.println(nama + " telah tiada");
			return;
		}
		int biaya = namaTempat.length() * 10000;
		if (uang > biaya) {
			uang -= biaya;
			kebahagiaan += namaTempat.length();
			System.out.println(nama + " berekreasi di " + namaTempat + ", " + nama + " senang :)");
			if (kebahagiaan > 100) {
				kebahagiaan = 100;
			}
		} else if (uang < biaya) {
			System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
		}
	}

	public void sakit(String namaPenyakit) {
		if (!(listNama.contains(this))) {
			System.out.println(nama + " telah tiada");
			return;
		}
		kebahagiaan -= namaPenyakit.length();
		if (kebahagiaan < 0) {
			kebahagiaan = 0;
		}
		System.out.println(nama + " terkena penyakit " + namaPenyakit + " :O");
	}

	public void meninggal() {
		int indexAkhir = listNama.size() - 1;
		if (!(listNama.contains(this))) {
			System.out.println(nama + " telah tiada");
			return;
		}
		if (this == listNama.get(indexAkhir)) {
			System.out.println(nama + " meninggal dengan tenang, kebahagiaan: " + kebahagiaan);
			System.out.println("Semua harta " + nama + " hangus");
		} else {
			System.out.println(nama + " meninggal dengan tenang, kebahagiaan: " + kebahagiaan);
			listNama.get(indexAkhir).setUang(listNama.get(indexAkhir).getUang() + uang);
			System.out.println("Semua harta " + nama + " disumbangkan untuk " + listNama.get(indexAkhir).getNama());
		}
		uang = 0;
		listNama.remove(this);
	}

	public String toString() {
		String namaOutput = nama;
		if (!listNama.contains(this)) {
			namaOutput = "Almarhum " + nama;
		}
		return ("Nama \t \t: " + namaOutput + "\n" + "Umur \t \t: " + umur + "\n" + "Uang \t \t: " + uang + "\n"
				+ "Kebahagiaan \t: " + kebahagiaan);
	}

	// untuk membandingkan object dengan object, diperlukan suatu method
	public boolean equals(Object o) {
		Manusia other = (Manusia) o;
		return this.nama.equals(other.nama);
	}
}

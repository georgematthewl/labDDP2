public class BingoCard {

    private Number[][] numbers;
    private boolean isBingo;

    public BingoCard(Number[][] numbers) {
        this.numbers = numbers;
        this.isBingo = false;
    }

    public Number[][] getNumbers() {
        return numbers;
    }

    public void setNumbers(Number[][] numbers) {
        this.numbers = numbers;
    }

    public boolean isBingo() {
        return isBingo;
    }

    public void setBingo(boolean isBingo) {
        this.isBingo = isBingo;
    }

    // Method untuk cek Bingo
    public void cekBingo() {
        // Cek horizontal dan vertikal
        for (int i = 0; i < numbers.length; i++) {
            int counterHorizontal = 0;
            int counterVertikal = 0;
            for (int j = 0; j < numbers.length; j++) {
                if (numbers[i][j].isChecked()) {
                    counterHorizontal += 1;
                }
                if (numbers[j][i].isChecked()) {
                    counterVertikal += 1;
                }
            }
            if (counterHorizontal == 5) {
                this.isBingo = true;
            }
            if (counterVertikal == 5) {
                this.isBingo = true;
            }
        }
        // Cek diagonal kanan dan kiri
        int counterDiagonalKanan = 0;
        int counterDiagonalKiri = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i][i].isChecked()) {
                counterDiagonalKanan += 1;
            }
            if (numbers[i][numbers.length - i - 1].isChecked()) {
                counterDiagonalKiri += 1;
            }
        }
        if (counterDiagonalKanan == 5) {
            this.isBingo = true;
        }
        if (counterDiagonalKiri == 5) {
            this.isBingo = true;
        }
    }

    public String markNum(int num, String nama) {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (num == numbers[i][j].getValue()) { // kalo udah di cross
                    if (numbers[i][j].isChecked()) {
                        return (nama  + ": " + num + " sebelumnya sudah tersilang");

                    } else { // kalo belom di cross
                        numbers[i][j].setChecked(true);
                        this.cekBingo();
                        return (nama + ": " + num + " tersilang");
                    }
                }
            }
        }
        return "Kartu tidak memiliki angka " + num;
    }

    public String info() {
        String printBingoCard = "";
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (j != 4) {
                    if (numbers[i][j].isChecked()) {
                        printBingoCard += "| X  ";
                    } else {
                        printBingoCard += ("| " + numbers[i][j].getValue() + " ");
                    }
                } else {
                    if (numbers[i][j].isChecked()) {
                        printBingoCard += "| X  |";
                    } else {
                        printBingoCard += ("| " + numbers[i][j].getValue() + " |");
                    }

                }
            }
            if (i != 4)
                printBingoCard += "\n";
        }
        return printBingoCard;
    }

    public void restart() {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                numbers[i][j].setChecked(false);
            }
        }
        System.out.println("Mulligan!");
    }

}

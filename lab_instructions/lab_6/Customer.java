import java.util.ArrayList;
public class Customer {

    private String nama;
    private int umur;
    boolean jenisKelamin;
    //private static ArrayList<Ticket> listTicketOrdered = new ArrayList<Ticket>();

    public Customer(String nama, Boolean jenisKelamin, int umur) {
        this.nama = nama;
        this.jenisKelamin = false; // cewe true, cowo false
        this.umur = umur;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public boolean getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(boolean jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public void findMovie(Theater theater, String judul) {
        int counter = 0;
        for (Movie film : theater.getDaftarFilm()) {
            if (judul.equals(film.getJudul())) {
                System.out.println("----------------------------------------------------------------");
                System.out.println("Judul \t: " + film.getJudul());
                System.out.println("Genre \t: " + film.getGenre());
                System.out.println("Durasi \t: " + film.getDurasi() + " menit");
                System.out.println("Rating \t: " + film.getRating());
                System.out.println("Jenis \t: Film " + film.getJenis());
                System.out.println("----------------------------------------------------------------");
            } else {
                counter += 1;
                if (counter == theater.getDaftarFilm().length) {
                    System.out.println("Film " + judul + " yang dicari " + this.nama + " tidak ada di bioskop "
                            + theater.getBioskop());
                }
            }
        }

    }

    public Ticket orderTicket(Theater theater, String judul, String jadwal, String jenisTiket) {
        int counter = 0;
        for (Ticket ticket : theater.getListTicket()) {
            if (ticket.getMovie().getJudul().equalsIgnoreCase(judul) && ticket.getJadwal().equalsIgnoreCase(jadwal)
                    && ticket.cek3D().equalsIgnoreCase(jenisTiket)) {
                if (umur >= ticket.getMovie().cekRating()) {
                    System.out.println(nama + " telah membeli tiket " + judul + " jenis " + jenisTiket + " di "
                            + theater.getBioskop() + " pada hari " + jadwal + " seharga Rp. " + ticket.hargaTiket());
                    theater.setSaldo(theater.getSaldo() + ticket.hargaTiket());
                    //listTicketOrdered.add(ticket);
                    return ticket;
                }
            } else {
                counter += 1;
                if (counter == theater.getListTicket().size()) {
                    System.out.println("Tiket untuk film " + judul + " jenis " + jenisTiket + " dengan jadwal " + jadwal
                            + " tidak tersedia di " + theater.getBioskop());
                }
            }

        }
        return null;
    }
/*
    public void cancelTicket(Theater theater) {
        if (theater.getListTicket().equals(listTicketOrdered.get(listTicketOrdered.size() - 1))) {
            
        }
    }

    public void watchMovie(Ticket tiket) {
        // Kalo filmnya uda ditonton, nyetak
        

    }
    */

}

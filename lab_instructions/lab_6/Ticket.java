
public class Ticket {
    private Movie movie;
    private String jadwal;
    private Theater bioskop;
    private boolean is3D;

    public Ticket(Movie movie, String jadwal, boolean is3D) {
        this.movie = movie;
        this.jadwal = jadwal;
        this.is3D = is3D;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Theater getBioskop() {
        return bioskop;
    }

    public void setBioskop(Theater bioskop) {
        this.bioskop = bioskop;
    }

    public boolean isIs3D() {
        return is3D;
    }

    public void setIs3D(boolean is3d) {
        is3D = is3d;
    }

    public String cek3D() {
        if (is3D) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }
    }

    public int hargaTiket() {
        int hargaStandar = 60000;
        if (jadwal.equalsIgnoreCase("Sabtu") || jadwal.equalsIgnoreCase("Minggu")) {
            if (is3D) {
                return (int) ((0.2 * (hargaStandar + 40000)) + (hargaStandar + 40000));
            } else {
                return hargaStandar += 40000;
            }
        } else {
            if (is3D) {
                return (int) ((0.2 * hargaStandar) + hargaStandar);
            } else {
                return hargaStandar;
            }
        }
    }

}

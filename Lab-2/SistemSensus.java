import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author George Matthew L, NPM 1706043891, Kelas C, GitLab Account: georgematthewl
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		
		String nama = "", alamat = "", tanggalLahir = "", catatan = "", tahunLahir="";
		String[] split_tanggal = null;
		short panjang = 0, lebar = 0, tinggi = 0;
		float berat = 0;
		byte jumlahCetakan = 0, jumlahAnggota = 0;
		
		try {
			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			alamat = input.nextLine();
			System.out.print("Panjang Tubuh (cm)     : ");
			panjang = input.nextShort();
			if(panjang < 0 || panjang > 250) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			System.out.print("Lebar Tubuh (cm)       : ");
			lebar = input.nextShort();
			if(lebar < 0 || lebar > 250) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			System.out.print("Tinggi Tubuh (cm)      : ");
			tinggi = input.nextShort();
			if(tinggi < 0 || tinggi > 250) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			System.out.print("Berat Tubuh (kg)       : ");
			berat = input.nextFloat();
			if(berat < 0 || berat > 150) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			System.out.print("Jumlah Anggota Keluarga: ");
			jumlahAnggota = input.nextByte();
			if(jumlahAnggota < 0 || jumlahAnggota > 20) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			input.nextLine();
			System.out.print("Tanggal Lahir          : ");
			tanggalLahir = input.nextLine();
			split_tanggal = tanggalLahir.split("-");
			tahunLahir = split_tanggal[2];
			for (String myStr: split_tanggal) {
				int ubah = Integer.parseInt(myStr);
				}
			if (Short.parseShort(tahunLahir) >= 2018) {
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
			System.out.print("Catatan Tambahan       : ");
			catatan = input.nextLine();
			System.out.print("Jumlah Cetakan Data    : ");
			jumlahCetakan = input.nextByte();
		} 
		catch (Exception e) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
	
		float rasioFloat = berat/(((float)panjang/100)*((float)lebar/100)*((float)tinggi/100));
		int rasio = (int) rasioFloat;
		
		input.nextLine();
		for (int counter = 1; counter <= jumlahCetakan; counter++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + counter + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak	
			if (catatan.equals("")) { 
				System.out.println("DATA SIAP DICETAK UNTUK\n" + "--------------------\n" + penerima + "\n" + nama + " - " + alamat + "\nLahir pada tanggal " + tanggalLahir + "\nRasio Berat Per Volume = " + rasio + " kg/m^3 "
						+ "\nTidak ada catatan tambahan");
			}
			else {
				System.out.println("DATA SIAP DICETAK UNTUK\n" + "--------------------\n" + penerima + "\n" + nama + " - " + alamat + "\nLahir pada tanggal " + tanggalLahir + "\nRasio Berat Per Volume = " + rasio + " kg/m^3 "
						+ "\nCatatan: " + catatan);
			}

		}

		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		char namaDepan = nama.charAt(0);
		char huruf; 
		int total = 0;
		for(int counter = 0; counter < nama.length(); counter++) {
			huruf = nama.charAt(counter);
			int ascii = (int) huruf;
			total += ascii;
		}
		int kalkulasi = ((panjang*lebar*tinggi) + total)%10000;
				
		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = namaDepan + "" + kalkulasi;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000*365*jumlahAnggota;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		int umur1 = 2018 - Short.parseShort(tahunLahir);
		byte umur = (byte) umur1;
		
		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApt = "", kabupaten = "";
		if(umur <= 18 && anggaran > 0) {
			namaApt = "PPMT";
			kabupaten = "Rotunda";
		}
		else if (umur >= 19 && umur <= 1018 && anggaran > 0 && anggaran <= 100000000) {
			namaApt = "Teksas";
			kabupaten = "Sastra";
		}
		else if (umur >= 19 && umur <= 1018 && anggaran > 0 && anggaran > 100000000) {
			namaApt = "Mares";
			kabupaten = "Margonda";
		}

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n" + "--------------------\n" + "MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga +
				"\nMENIMBANG: Anggaran makan tahunan: Rp " + anggaran + "\n          " + " Umur kepala keluarga: " + umur + " tahun" +
				"\nMEMUTUSKAN: keluarga " + nama + " akan ditempatkan di: " +"\n" + namaApt + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);
		input.close();
	}
}
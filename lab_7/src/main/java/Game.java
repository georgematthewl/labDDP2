import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> eaten = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player nama : player) {
            if (nama.getName().equalsIgnoreCase(name)) {
                return nama;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        Player character = find(chara);
        if (!cekPlayer(character)) {
            if (tipe.equalsIgnoreCase("Human")) {
                Human newHuman = new Human(chara, hp);
                player.add(newHuman);
            } else if (tipe.equalsIgnoreCase("Magician")) {
                Magician newMagician = new Magician(chara, hp);
                player.add(newMagician);
            } else if (tipe.equalsIgnoreCase("Monster")) {
                Monster newMonster = new Monster(chara, hp);
                player.add(newMonster);
            }

        } else {
            return "Sudah ada karakter bernama " + chara;
        }
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        Player character = find(chara);
        if (cekPlayer(character)) {
            Monster newMonster = new Monster(chara, hp, roar);
            player.add(newMonster);
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player character = find(chara);
        if (cekPlayer(character)) {
            player.remove(find(chara));
        } else {
            return "Tidak ada " + chara;
        }
        return chara + " dihapus dari game";
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player character = find(chara);
        String print = null;
        if (cekPlayer(character)) {
            if (find(chara).getHp() <= 0) {
                print = find(chara).getJenis() + " " + find(chara).getName() + "\nHP: 0\n"
                        + "Sudah meninggal dunia dengan damai\n" + this.diet(chara);
            } else {
                print = find(chara).getJenis() + " " + find(chara).getName() + "\nHP: " + find(chara).getHp() + "\n"
                        + "Masih hidup\n" + this.diet(chara);
            }
        }
        return print;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String print = "";
        if (player.size() != 0) {
            for (Player listPlayer : player) {
                print += status(listPlayer.getName()) + "\n";
            }
            return print;
        }
        return "Tidak ada pemain";
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        Player character = find(chara);
        character.sortList();
        String tmp = "Memakan ";
        if (cekPlayer(character)) {
            if (character.getListPlayerDiet().size() == 0) {
                return "Belum memakan siapa siapa";
            } else {
                int count = 0;
                for (Player player : character.getListPlayerDiet()) {
                    if (count == character.getListPlayerDiet().size() - 1) {
                        tmp += player.getJenis() + " " + player.getName();
                    } else {
                        tmp += player.getJenis() + " " + player.getName() + ", ";
                        count++;
                    }
                }
                return tmp;
            }
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String print = "Termakan: ";
        if (eaten.size() != 0) {
            int count = 0;
            for (Player list : eaten) {
                if (count == (eaten.size() - 1)) {
                    print += list.getJenis() + " " + list.getName();
                } else {
                    print += list.getJenis() + " " + list.getName() + ", ";
                    count++;
                }
            }
            return print;
        }
        return "Belum ada yang termakan";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player character = find(meName);
        Player enemy = find(enemyName);
        if (cekPlayer(character) && cekPlayer(enemy)) {
            if (character.getHp() > 0) {
                character.attack(enemy);
            } else {
                return meName + " tidak dapat menyerang " + enemyName;
            }

        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        return "Nyawa " + enemyName + " " + enemy.getHp();
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player character = find(meName);
        Player enemy = find(enemyName);
        String print = "";
        if (character instanceof Magician && cekPlayer(enemy)) {
            if (character.getHp() > 0) {
                ((Magician) character).burn(enemy);
                print = "Nyawa " + enemyName + " " + enemy.getHp();
                if (enemy.getHp() <= 0) {
                    print += " dan matang";
                }
            } else {
                print = meName + " tidak dapat membakar " + enemyName;
            }
        } else {
            print = "Tidak ada " + meName + " atau " + enemyName;
        }
        return print;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player character = find(meName);
        Player enemy = find(enemyName);
        if (cekPlayer(character) && cekPlayer(enemy)) {
            if (character.getHp() > 0 && character.canEat(enemy)) {
                character.eat(enemy);
                eaten.add(enemy);
                player.remove(enemy);
                return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + character.getHp();
            } else {
                return meName + " tidak bisa memakan " + enemyName;
            }
        }
        return "Tidak ada " + meName + " atau " + enemyName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player character = find(meName);
        if (cekPlayer(character)) {
            if (character.getHp() > 0 && character instanceof Monster) {
                return ((Monster) character).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
        return "Tidak ada " + meName;
    }

    public String tree(Player character) {
        String result = "";
        if (character.getListPlayerDiet().size() == 0) {
            result += character.getName();
        } else {
            result += character.getName() + " <-- (";
            for (int i = 0; i < character.getListPlayerDiet().size(); i++) {
                if (i == character.getListPlayerDiet().size() - 1) {
                    result += tree(character.getListPlayerDiet().get(i));
                } else {
                    result += tree(character.getListPlayerDiet().get(i)) + " | ";
                }
            }
            result += ")";
        }
        return result;
    }

    public void cetakMenu() {
        for (Player character : player) {
            System.out.println(tree(character));
        }
    }

    public boolean cekPlayer(Player me) {
        if (me == null)
            return false;
        else
            return true;
    }

}

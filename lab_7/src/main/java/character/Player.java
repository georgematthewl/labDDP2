package character;

import java.util.ArrayList;

public class Player {
    private String name, jenis;
    private int hp;
    private ArrayList<Player> listPlayerDiet = new ArrayList<>();
    private boolean isBurned;
    private int id;
    public static int player_id = 0;
    private static final int MAGICIAN_GOT_ATTACKED = 20;
    private static final int TARGET_GOT_ATTACKED = 10;
    private static final int EARN_HP = 15;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        this.isBurned = false;
        if (this instanceof Human)
            this.jenis = "Human";
        else if (this instanceof Magician)
            this.jenis = "Magician";
        else if (this instanceof Monster)
            this.jenis = "Monster";
        this.id = ++player_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public ArrayList<Player> getListPlayerDiet() {
        return listPlayerDiet;
    }

    public void setListPlayerDiet(ArrayList<Player> listPlayerDiet) {
        this.listPlayerDiet = listPlayerDiet;
    }

    public boolean isBurned() {
        return isBurned;
    }

    public void setBurned(boolean isBurned) {
        this.isBurned = isBurned;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public void attack(Player target) {
        if (target instanceof Magician) {
            target.setHp(target.getHp() - MAGICIAN_GOT_ATTACKED);
            if (target.getHp() <= 0) {
                target.setHp(0);
            }
        } else {
            target.setHp(target.getHp() - TARGET_GOT_ATTACKED);
            if (target.getHp() <= 0) {
                target.setHp(0);
            }
        }
    }

    public void eat(Player target) {
        this.hp += EARN_HP;
        listPlayerDiet.add(target);
    }

    public boolean canEat(Player target) {
        if (this instanceof Monster) {
            if (target.getHp() == 0) {
                return true;
            }
        } else if (this instanceof Human || this instanceof Magician) {
            if (target instanceof Monster && target.isBurned == true) {
                return true;
            }

        }
        return false;
    }
    public void sortList() {
      for (int a = 0; a < listPlayerDiet.size(); a++) {
        for (int b = a; b < listPlayerDiet.size(); b++) {
          if (listPlayerDiet.get(b).id < listPlayerDiet.get(a).id) {
            Player tmp = listPlayerDiet.get(b);
            listPlayerDiet.set(b, listPlayerDiet.get(a));
            listPlayerDiet.set(a, tmp);
          }
        }
      }
    }
}

package character;

public class Magician extends Player {
    public Magician(String name, int hp) {
        super(name, hp);
    }

    public void burn(Player target) {
        super.attack(target);
        if (target.getHp() == 0) {
            target.setBurned(true);
        }
    }

}
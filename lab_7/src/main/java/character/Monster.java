package character;

public class Monster extends Player {
    private String roaring;

    public Monster(String name, int hp) {
        this(name, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public Monster(String name, int hp, String roaring) {
        super(name, hp * 2);
        this.roaring = roaring;
    }

    public String getRoaring() {
        return roaring;
    }

    public void setRoaring(String roaring) {
        this.roaring = roaring;
    }

    public String roar() {
        return this.roaring;
    }

}

package theater;

import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Theater {
    private String bioskop;
    private Movie[] daftarFilm;
    private int saldo;
    private ArrayList<Ticket> listTicket;

    public Theater(String bioskop, int saldo, ArrayList<Ticket> listTicket, Movie[] daftarFilm) {
        this.bioskop = bioskop;
        this.saldo = saldo;
        this.listTicket = listTicket;
        this.daftarFilm = daftarFilm;
    }

    public String getBioskop() {
        return bioskop;
    }

    public void setBioskop(String bioskop) {
        this.bioskop = bioskop;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public Movie[] getDaftarFilm() {
        return daftarFilm;
    }

    public void setDaftarFilm(Movie[] daftarFilm) {
        this.daftarFilm = daftarFilm;
    }

    public ArrayList<Ticket> getListTicket() {
        return listTicket;
    }

    public void setListTicket(ArrayList<Ticket> listTicket) {
        this.listTicket = listTicket;
    }

    public boolean cekMovie(Movie movie) {
        for (Movie film : daftarFilm) {
            if (film.equals(movie)) {
                return true;
            }
        }
        return false;
    }

    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop \t\t: " + bioskop);
        System.out.println("Saldo Kas \t\t: " + saldo);
        System.out.println("Jumlah Tiket Tersedia \t: " + listTicket.size());
        String film = "";
        for (int i = 0; i < daftarFilm.length; i++) {
            if (i == daftarFilm.length - 1) {
                film += daftarFilm[i].getJudul();
            } else {
                film += daftarFilm[i].getJudul() + ", ";
            }
        }
        System.out.println("Daftar Film Tersedia \t: " + film);
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theaters) {
        int totalUang = 0;
        for (Theater bioskop : theaters) {
            totalUang += bioskop.getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalUang);
        System.out.println("------------------------------------------------------------------");
        for (Theater bioskop : theaters) {
            System.out.println("Bioskop \t: " + bioskop.getBioskop());
            System.out.println("Saldo Kas \t: Rp. " + bioskop.getSaldo());
            System.out.println("\n");
        }
        System.out.println("------------------------------------------------------------------");
    }

}

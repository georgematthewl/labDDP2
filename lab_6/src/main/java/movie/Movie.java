package movie;

public class Movie {
    private String judul, genre, rating, jenis;
    private int durasi;

    public Movie(String judul, String rating, int durasi, String genre, String jenis) {
        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDurasi() {
        return durasi;
    }

    public void setDurasi(int durasi) {
        this.durasi = durasi;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    @Override
    public boolean equals(Object o) {
        return this == (Movie) o;
    }

    public int cekRating() {
        if (rating.equalsIgnoreCase("Remaja")) {
            return 13;
        } else if (rating.equalsIgnoreCase("Dewasa")) {
            return 17;
        } else if (rating.equalsIgnoreCase("Umum")) {
            return 0;
        }
        return 0;
    }

}

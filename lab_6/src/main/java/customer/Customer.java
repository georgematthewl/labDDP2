package customer;

import java.util.ArrayList;
import theater.Theater;
import ticket.Ticket;
import movie.Movie;

public class Customer {

    private String nama, jenisKelamin;
    private int umur;
    private ArrayList<Ticket> listTicketOrdered = new ArrayList<Ticket>();
    private ArrayList<Ticket> listMovieWatched = new ArrayList<Ticket>();

    public Customer(String nama, String jenisKelamin, int umur) {
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public void findMovie(Theater theater, String judul) {
        int counter = 0;
        for (Movie film : theater.getDaftarFilm()) {
            if (judul.equals(film.getJudul())) {
                System.out.println("------------------------------------------------------------------");
                System.out.println("Judul \t: " + film.getJudul());
                System.out.println("Genre \t: " + film.getGenre());
                System.out.println("Durasi \t: " + film.getDurasi() + " menit");
                System.out.println("Rating \t: " + film.getRating());
                System.out.println("Jenis \t: Film " + film.getJenis());
                System.out.println("------------------------------------------------------------------");
            } else {
                counter++;
                if (counter == theater.getDaftarFilm().length) {
                    System.out.println(
                            "Film " + judul + " yang dicari " + nama + " tidak ada di bioskop " + theater.getBioskop());
                }
            }
        }

    }

    public Ticket orderTicket(Theater theater, String judul, String jadwal, String jenisTiket) {
        int counter = 0;
        for (Ticket ticket : theater.getListTicket()) {
            if (ticket.getMovie().getJudul().equalsIgnoreCase(judul) && ticket.getJadwal().equalsIgnoreCase(jadwal)
                    && ticket.cek3D().equalsIgnoreCase(jenisTiket)) {
                if (umur >= ticket.getMovie().cekRating()) {
                    System.out.println(nama + " telah membeli tiket " + judul + " jenis " + jenisTiket + " di "
                            + theater.getBioskop() + " pada hari " + jadwal + " seharga Rp. " + ticket.hargaTiket());
                    theater.setSaldo(theater.getSaldo() + ticket.hargaTiket());
                    listTicketOrdered.add(ticket);
                    return ticket;
                } else {
                    System.out.println(nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating "
                            + ticket.getMovie().getRating());
                }
            } else {
                counter++;
                if (counter == theater.getListTicket().size()) {
                    System.out.println("Tiket untuk film " + judul + " jenis " + jenisTiket + " dengan jadwal " + jadwal
                            + " tidak tersedia di " + theater.getBioskop());
                }
            }

        }
        return null;
    }

    public void cancelTicket(Theater theater) {
        Ticket lastTicket = listTicketOrdered.get(listTicketOrdered.size() - 1);
        if (listMovieWatched.contains(lastTicket)) { // cek apakah udah di tonton
            System.out.println("Tiket tidak bisa dikembalikan karena film " + lastTicket.getMovie().getJudul()
                    + " sudah ditonton oleh " + nama);
        } else if (!(theater.cekMovie(lastTicket.getMovie()))) { // cek ada di daftar film di theater tsb
            System.out.println("Maaf tiket tidak bisa dikembalikan, " + lastTicket.getMovie().getJudul()
                    + " tidak tersedia dalam " + theater.getBioskop());
        } else if (theater.getSaldo() - lastTicket.hargaTiket() < 0) { // saldo di bioskop kurang
            System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " + theater.getBioskop()
                    + " lagi tekor...");
        } else {
            System.out.println(
                    "Tiket film " + lastTicket.getMovie().getJudul() + " dengan waktu tayang " + lastTicket.getJadwal()
                            + " jenis " + lastTicket.cek3D() + " dikembalikan ke bioskop " + theater.getBioskop());
            theater.setSaldo(theater.getSaldo() - lastTicket.hargaTiket());
            listTicketOrdered.remove(lastTicket);
        }
    }

    public void watchMovie(Ticket tiket) {
        if (listTicketOrdered.contains(tiket) && !(listMovieWatched.contains(tiket))) {
            System.out.println(nama + " telah menonton film " + tiket.getMovie().getJudul());
            listMovieWatched.add(tiket);
        } else if (listMovieWatched.contains(tiket)) {
            System.out.println("Film " + tiket.getMovie().getJudul() + " sudah ditonton sebelumnya");
        } else {
            System.out.println("Maaf, tiket untuk film " + tiket.getMovie().getJudul() + " belum dibeli");
        }

    }

}

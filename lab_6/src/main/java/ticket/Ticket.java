package ticket;

import movie.Movie;
import theater.Theater;

public class Ticket {
    private Movie movie;
    private String jadwal;
    private boolean is3D;
    private static final int HARGA_TIKET = 60000;

    public Ticket(Movie movie, String jadwal, boolean is3D) {
        this.movie = movie;
        this.jadwal = jadwal;
        this.is3D = is3D;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public boolean isIs3D() {
        return is3D;
    }

    public void setIs3D(boolean is3d) {
        is3D = is3d;
    }

    public void printTicket() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Film \t\t\t:" + movie.getJudul());
        System.out.println("Jadwal Tayang \t\t: " + jadwal);
        System.out.println("Jenis \t\t\t: " + cek3D());
        System.out.println("------------------------------------------------------------------");
    }

    public String cek3D() {
        if (is3D) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }
    }

    public int hargaTiket() {
        int hargaStandar = HARGA_TIKET;
        if (jadwal.equalsIgnoreCase("Sabtu") || jadwal.equalsIgnoreCase("Minggu")) {
            if (is3D) {
                return (int) ((0.2 * (hargaStandar + 40000)) + (hargaStandar + 40000));
            } else {
                return hargaStandar += 40000;
            }
        } else {
            if (is3D) {
                return (int) ((0.2 * hargaStandar) + hargaStandar);
            } else {
                return hargaStandar;
            }
        }
    }

}
